import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

CHARTS_FOLDER = '/srv/charts/'
HELM_PATH = '/usr/local/bin/helm'
ALLOWED_EXTENSIONS = { 'tgz' }

helm = Flask(__name__,
            static_url_path='', 
            static_folder=CHARTS_FOLDER,
            )
helm.config['CHARTS_FOLDER'] = CHARTS_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@helm.route('/publish', methods=['POST'])
def upload_file():
    print(request.files)
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(helm.config['CHARTS_FOLDER'], filename))
        res = os.system(f'sh -c "cd {CHARTS_FOLDER} ; {HELM_PATH} repo index ."')
        return f'Index returned {res}'

if __name__ == "__main__":
    helm.run(host='0.0.0.0')
