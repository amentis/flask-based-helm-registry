import os
import tempfile
import tarfile
from flask import Flask, flash, request, redirect
from werkzeug.utils import secure_filename
from pathlib import Path
import shutil

MANIFESTS_FOLDER = '/srv/manifests/'
ALLOWED_EXTENSIONS = { 'tgz' }
KUSTOMIZE_PATH = '/usr/local/bin/kustomize'

kustomize = Flask(__name__,
            static_url_path='',
            static_folder=MANIFESTS_FOLDER,
            )
kustomize.config['MANIFESTS_FOLDER'] = MANIFESTS_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@kustomize.route('/publish', methods=['POST'])
def upload_file():
    print(request.files)
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        tmpfile = os.path.join(tempfile.gettempdir(), filename)
        tmpfile_no_extension=filename.rsplit('.',1)[0]
        tmpfile_no_version=tmpfile_no_extension.rsplit('-',1)[0]
        tmpdir = os.path.join(tempfile.gettempdir(), tmpfile_no_extension)
        finaldir = os.path.join(kustomize.config['MANIFESTS_FOLDER'], tmpfile_no_extension)
        Path(tmpdir).mkdir(parents=True, exist_ok=True)
        Path(finaldir).mkdir(parents=True, exist_ok=True)
        file.save(tmpfile)
        with tarfile.open(tmpfile, "r:gz") as tar:
            tar.extractall(tmpdir)
        os.remove(tmpfile)
        script_body = \
        """
        set -x
        RETURN_TO="$(pwd)"

        cd {tmpdir}/{tmpfile_no_version}/overlays
        ls | xargs -L 1 -I{{}} kustomize build {{}} -o {finaldir}/{{}}.yaml

        cd $RETURN_TO
        """.format(tmpdir=tmpdir, tmpfile_no_version=tmpfile_no_version, finaldir=finaldir)
        os.system(f'bash -c "{script_body}"')
        shutil.rmtree(tmpdir)
    return 'OK'

@kustomize.route('/latest/<application>', methods=['GET'])
def latest(application):
    files = os.listdir(kustomize.config['MANIFESTS_FOLDER'])
    filtered = filter(lambda s: application in s, files)
    versions = list(map(lambda s: s.split('-')[-1], filtered))
    versions.sort(reverse=True)
    return versions[0]

if __name__ == "__main__":
    kustomize.run(host='0.0.0.0')
