#!/usr/bin/env bash
PIP="python3 -m pip"

$PIP install wheel flask uwsgi

source config.env

sed "s/<HELM_URL>/${HELM_URL}/g" nginx.conf.raw > nginx.conf
sed -i.bck "s/<KUSTOMIZE_URL>/${KUSTOMIZE_URL}/g" nginx.conf

cp flask-based-helm-registry.service /etc/systemd/system/
cp flask-based-kustomize-registry.service /etc/systemd/system/

systemctl daemon-reload
systemctl start flask-based-helm-registry
systemctl enable flask-based-helm-registry
systemctl restart flask-based-helm-registry

systemctl daemon-reload
systemctl start flask-based-kustomize-registry
systemctl enable flask-based-kustomize-registry
systemctl restart flask-based-kustomize-registry

cp nginx.conf /etc/nginx/nginx.conf
nginx -t
systemctl restart nginx
